import React, { useEffect, useState } from 'react';
import { GoogleMap, useLoadScript, Marker } from '@react-google-maps/api';
import styles from './Map.module.css';

export function Map(props: { busStop: boolean }) {
    // In production practice, .env file, which is ignored by git, is used to store google maps api key instead of directly putting the key here 
    const { isLoaded } = useLoadScript({ googleMapsApiKey: "AIzaSyDVAp3UE09hG7g6UlZ6LvDNEKZh_oRhZQM" });
    const [lat, setLat] = useState<number>(0);
    const [lng, setLng] = useState<number>(0);

    async function getStopData() {
        let busStopId: string = '';
        if (props.busStop === true) {
            busStopId = 'BFA3460955AC820C';
        } else {
            busStopId = '5FB1FCAF80F3D97D';
        }

        try {
            const res = await fetch('https://data.etabus.gov.hk/v1/transport/kmb/stop/' + busStopId);
            const json = await res.json();
            setLat(lat => lat = +json.data.lat);
            setLng(lng => lng = +json.data.long);
        } catch (error) {
            console.error('error: ', error);
        };
    };

    useEffect(() => {
        getStopData()
    }, [props.busStop]);

    return (
        (!isLoaded)
            ? <div> Loading....</div>
            : <GoogleMap
                zoom={17}
                center={{ lat: lat, lng: lng }}
                mapContainerClassName={styles.mapContainer}
            >
                <Marker
                    position={{ lat: lat, lng: lng }} />
            </GoogleMap>
    );
};
