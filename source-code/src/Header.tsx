import React, { useState } from 'react';
import styles from './Header.module.css';

export function Header(props: { onClick: () => void }) {
    const [tkw, setTkw] = useState<boolean>(true);

    return (
        <div className={styles.header}>
            <div className={styles.stop}
                onClick={() => {
                    setTkw(tkw => !tkw);
                    props.onClick();
                }}> {tkw
                    ? '荃景圍天橋'
                    : '荃灣柴灣角街'}
            </div>
        </div>
    );
}
