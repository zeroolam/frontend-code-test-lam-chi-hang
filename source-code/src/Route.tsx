import React, { useEffect, useState } from 'react';
import styles from './Route.module.css'
import { Modal } from '@mantine/core';
import { Map } from './Map';

type Props = {
    routeNo: string,
    destTc: string,
    eta: string,
    busStop: boolean,
    busStopId: string,
    serviceType: number
}

export function Route(props: Props) {
    const [opened, setOpened] = useState<boolean>(false);
    const [routeEta, setRouteEta] = useState<any[]>([]);

    function calculateEta(value: string) {
        const date = new Date();
        let eta: number = (new Date(value).getMinutes()) - (date.getMinutes());
        if (eta < 0 && ((new Date(value).getHours() - date.getHours()) === 1)) {
            eta = eta + 60;
        };
        return eta;
    };

    async function getETA() {
        try {
            const res = await fetch(` https://data.etabus.gov.hk/v1/transport/kmb/eta/${props.busStopId}/${props.routeNo}/${props.serviceType}`);
            const json = await res.json();
            json.data.map((item: {}) => setRouteEta(routeEta => [...routeEta, item]));
        } catch (error) {
            console.error('error: ', error);
        };
    };

    useEffect(() => {
        setRouteEta([]);
        getETA();
    }, [props.routeNo]);

    return (
        <>
            <div className={styles.routeInfo} onClick={() => setOpened(true)}>
                <div className={styles.route}>{props.routeNo}</div>
                <div className={styles.destInfo}>
                    <div><span className={styles.to}>往 </span><span className={styles.dest}>{props.destTc}</span></div>
                    <div className={styles.busStop}>{props.busStop ? '荃景圍天橋' : '荃灣柴灣角街'}</div>
                </div>
                <div>
                    <div className={styles.eta}>{props.eta === null || calculateEta(props.eta) === 0 ? '-' : calculateEta(props.eta)}</div>
                    <div className={styles.etaUnit}>分鐘</div>

                </div>
            </div>

            <Modal centered
                opened={opened}
                onClose={() => setOpened(false)}
                withCloseButton={false}
                padding={0}>
                <Map busStop={props.busStop} />
                <hr className={styles.divider}></hr>
                <div className={styles.modalInfo}>
                    <div>巴士站 : {props.busStop ? '荃景圍天橋' : '荃灣柴灣角街'}</div>
                    <div>路線 {props.routeNo}</div>
                    {routeEta.map(item =>
                        <div key={routeEta.indexOf(item)} className={styles.threeEta}>
                            <span className={styles.eta}>
                                {calculateEta(item.eta) === 0 || item.eta === null
                                    ? '-'
                                    : calculateEta(item.eta)
                                }
                            </span>
                            <span className={styles.etaUnit}> 分鐘</span>
                            <span className={styles.expected}>{routeEta.indexOf(item) === 0
                                ? null
                                : ' 預定班次'}</span>
                        </div>
                    )}
                </div>
            </Modal>
        </>
    );
};
