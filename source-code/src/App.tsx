import React, { useEffect, useState } from 'react';
import styles from './App.module.css';
import { Header } from './Header';
import { Route } from './Route';
import PullToRefresh from 'react-simple-pull-to-refresh';

export default function App() {
  const [routes, setRoutes] = useState<any[]>([]);
  const [tkw, setTkw] = useState<boolean>(true);

  async function getRouteList() {
    let busStopId: string = '';
    if (tkw === true) {
      busStopId = 'BFA3460955AC820C';
    } else {
      busStopId = '5FB1FCAF80F3D97D';
    }
    try {
      const res = await fetch('https://data.etabus.gov.hk/v1/transport/kmb/stop-eta/' + busStopId);
      const json = await res.json();
      json.data.map((item: {}) => setRoutes(routes => [...routes, item]));
    } catch (error) {
      console.error('error: ', error);
    };
  };

  useEffect(() => {
    setRoutes([])
    getRouteList();
  }, [tkw]);

  return (
    <PullToRefresh onRefresh={getRouteList}>
      <div className={styles.app}>
        <Header onClick={() => setTkw(tkw => !tkw)} />
        <div className={styles.routeList}>
          <div className={styles.route}>
            {routes.map(item => (
              <Route
                key={routes.indexOf(item)}
                routeNo={item.route}
                destTc={item.dest_tc}
                eta={item.eta}
                busStop={tkw}
                busStopId={tkw ? 'BFA3460955AC820C' : '5FB1FCAF80F3D97D'}
                serviceType={item.service_type} />
            ))}
          </div>
        </div>
      </div>
    </PullToRefresh>
  );
}