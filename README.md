# Installation and execution steps
1. Clone the public git repository to the local machine
2. Open the repository with VS Code
3. Open the terminal in VS Code
4. Enter `cd ./source-code` in terminal
5. Enter `npm i` in terminal
6. Enter `npm start` in terminal
7. The development server is being start and the browser will be opened automatically
   The web application will be shown when loading is completed.

# Assumptions you have taken into account
1. Network connection is successfully connected without any failure
2. The components are rendered once only.    
3. All the required information is included in the project repository. 

# Choice of solutions and justification
For assumption 2, strict mode is disabled in order to render the components once only. 
It is because the components are rendered twice in development stage while the strict mode in react 18 is enabled.

For assumption 3, sensitive information is directly put in the project file instead of storing it in the .env file and being read using dotenv package
It is because .env file is ignored by git to protect the private and sensitive information 